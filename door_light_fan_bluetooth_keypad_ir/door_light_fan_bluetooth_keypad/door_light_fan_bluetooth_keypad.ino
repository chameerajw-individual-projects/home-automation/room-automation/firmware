char data = 0;
int fan = 10, light = 14, lockMotor = 9, countFan = 0, countLight = 0,
    countDoor = 0, wrongPassword = 0, buzzerOff = 12;

//if password is correct countDoor=1;
// when door open buzzer pin writes high and shutdowns the buzzer




#include <IRremote.h>
int recievePin = 13;
IRrecv irrecv(recievePin);
decode_results results;



#include <Keypad.h>
char* password = "465";
const byte rows = 4;
const byte coloumns = 3;
char keys[rows][coloumns] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};
byte rowPins[rows] = {8, 7, 6, 5};
byte colPins[coloumns] = {4, 3, 2};
Keypad keypad = Keypad(makeKeymap(keys),
                       rowPins, colPins, rows, coloumns );
int position = 0; //position of the password



void setup() {
  Serial.begin(9600);

  pinMode(light, OUTPUT);
  pinMode(fan, OUTPUT);
  pinMode(buzzerOff, OUTPUT);
  pinMode(lockMotor, OUTPUT);

  irrecv.enableIRIn();
}



void loop() {
  IR_remote();// processign remote data

  if (Serial.available() > 0) { // processign remote data
    data = Serial.read();
    Bluetooth_Module();
  }

  process_Keypad();

  Switching();

  //Serial.println(millis());
  //digitalWrite(lockMotor, LOW);
  //delay(50);
}

int IR_remote() {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    switch (results.value) {
      case 0xFA3F159F: //light on and off
        if (countLight == 0) {
          countLight = 1;
          delay(250);
          break;         
        }
        else if (countLight == 1) {
          countLight = 0;
          delay(250);
          break;
        }
        break;

      case 0x12209C7B: //fan on and off
        if (countFan == 0) {
          countFan = 1;
          delay(250);
          break;
        }
        else if (countFan == 1) {
          countFan = 0;
          delay(250);
          break;
        }
        break;

      case 0x550C590://enter to the room
        countDoor = 1;
        break;
    }

    irrecv.resume();
  }

}


int  Bluetooth_Module() {
  Serial.println(data);
  if (data == 'A')
    countLight = 1;
  else if (data == 'a')
    countLight = 0;
  if (data == 'B')
    countFan = 1;
  else if (data == 'b')
    countFan = 0;
  if (data == 'C')
    countDoor = 1;
  else if (data == 'c')
    countDoor = 0;
  return (countLight, countFan);
}


int process_Keypad() {

  char key = keypad.getKey();
  if (key == '0') {
    position = 0; //make password position to zero
    wrongPassword = 0;
  }
  if (key == password[position])
  {
    position++;  //if position of the passwordd is correct increment the position
  }
  else if ( key != NO_KEY ) { //if wrong password make passsword position to zero and increment wrong password
    if ( key != password[position]) {
      position = 0;
      wrongPassword++;
    }
  }
  if (position == 3) {
    countDoor = 1;
    position = 0;
  }
  if (key == '*') {
    wrongPassword--;
    if (countLight == 0) {
      countLight = 1;
    }
    else if (countLight == 1) {
      countLight = 0;
    }
  }
  if (key == '#') {
    wrongPassword--;
    if (countFan == 0) {
      countFan = 1;
    }
    else if (countFan == 1) {
      countFan = 0;
    }
  }
  return (countLight, countFan, countDoor);
}


void Switching() {
  if (countLight == 1) {
    digitalWrite(light, HIGH);
    //delay(50);
  }
  else if (countLight == 0) {
    digitalWrite(light, LOW);
    //delay(50);
  }
  if (countFan == 1) {
    digitalWrite(fan, HIGH);
    //delay(50);
  }
  else if (countFan == 0) {
    digitalWrite(fan, LOW);
    //delay(50);
  }
  if (countDoor == 1) {
    digitalWrite(lockMotor, HIGH);
    delay(1000);
    digitalWrite(lockMotor, LOW);
    delay(4000);
    countDoor = 0;
    countLight = 1;
    countFan = 1;
  }
  if (wrongPassword == 3) {
    delay(10000);
    wrongPassword = 0;
  }
}

