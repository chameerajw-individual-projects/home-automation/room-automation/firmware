 #include <IRremote.h>
char data = 0;
int fan=3;
int lit=2;
int RECV_PIN = 4;
int cntF =0;
int cntL =0;
int on=13;
IRrecv irrecv(RECV_PIN);

decode_results results;

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  pinMode(lit,OUTPUT);
  pinMode(fan,OUTPUT);
   pinMode(on, OUTPUT);
}

void loop() {
  

  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    irrecv.resume();
    // Receive the next value
  }
  
  if(Serial.available() > 0)  // Send data only when you receive data:
  {
    data = Serial.read();      //Read the incoming data and store it into variable data
    Serial.print(data);        //Print Value inside data in Serial monitor
    Serial.print("\n");        //New line 
    if(data == 'A')            //Checks whether value of data is equal to 1 
      cntL=1;  //If value is 1 then LED turns ON
    else if(data == 'a')       //Checks whether value of data is equal to 0
      cntL=0;   //If value is 0 then LED turns OFF
       if(data == 'B')            //Checks whether value of data is equal to 1 
      cntF=1;  //If value is 1 then LED turns ON
    else if(data == 'b')       //Checks whether value of data is equal to 0
      cntF=0;
  }
switch(results.value){
  case 0xFF22DD: 
    cntL=1;
    break;
  case 0xFFC23D:
    cntL=0;
    break; 
  case 0xFF629D: 
    cntF=1;
    break;
  case 0xFFA857:
    cntF=0;
    break;
}
 if (cntL==1){
  digitalWrite(lit,HIGH);
  }
 else if (cntL==0){
  digitalWrite(lit,LOW);
  }
  
if (cntF==1){
  digitalWrite(fan,HIGH);
  }
 else if (cntF==0){
  digitalWrite(fan,LOW);
  }
digitalWrite(on,HIGH); 
delay(100);
digitalWrite(on,LOW);
delay(500);
Serial.print(cntL,cntF);
}
